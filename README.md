<!-- BEGINNING OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.7.3 |
| <a name="requirement_ionoscloud"></a> [ionoscloud](#requirement\_ionoscloud) | 6.4.10 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_ionoscloud"></a> [ionoscloud](#provider\_ionoscloud) | 6.4.10 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [ionoscloud_datacenter.datacenter](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.10/docs/resources/datacenter) | resource |
| [ionoscloud_ipblock.example](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.10/docs/resources/ipblock) | resource |
| [ionoscloud_lan.database_lan](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.10/docs/resources/lan) | resource |
| [ionoscloud_lan.monitoring_lan](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.10/docs/resources/lan) | resource |
| [ionoscloud_lan.nodes_lan](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.10/docs/resources/lan) | resource |
| [ionoscloud_lan.private_lan](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.10/docs/resources/lan) | resource |
| [ionoscloud_lan.public_lan](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.10/docs/resources/lan) | resource |
| [ionoscloud_lan.storage_lan](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.10/docs/resources/lan) | resource |
| [ionoscloud_natgateway.example](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/6.4.10/docs/resources/natgateway) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_description"></a> [description](#input\_description) | Short Description of the VDC, for example usage type or environments | `string` | n/a | yes |
| <a name="input_ip_block_name"></a> [ip\_block\_name](#input\_ip\_block\_name) | (Optional) The name of Ip Block | `string` | n/a | yes |
| <a name="input_location"></a> [location](#input\_location) | Ionos VDC Location | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | Ionos VDC Name | `string` | n/a | yes |
| <a name="input_natgateway_name"></a> [natgateway\_name](#input\_natgateway\_name) | (Required)[string] Name of the NAT gateway. | `string` | n/a | yes |
| <a name="input_sec_auth_protection"></a> [sec\_auth\_protection](#input\_sec\_auth\_protection) | Enforce 2AF for the VDC | `bool` | n/a | yes |
| <a name="input_size"></a> [size](#input\_size) | (Required) The number of IP addresses to reserve for this block. | `number` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_database_lan"></a> [database\_lan](#output\_database\_lan) | The ID of the database lan |
| <a name="output_monitoring_lan"></a> [monitoring\_lan](#output\_monitoring\_lan) | The ID of the monitoring lan |
| <a name="output_nodes_lan"></a> [nodes\_lan](#output\_nodes\_lan) | The ID of the k8s-nodes lan |
| <a name="output_private_lan"></a> [private\_lan](#output\_private\_lan) | The ID of the private lan |
| <a name="output_public_lan"></a> [public\_lan](#output\_public\_lan) | The ID of the public lan |
| <a name="output_storage_lan"></a> [storage\_lan](#output\_storage\_lan) | The ID of the storage lan |
| <a name="output_vcd_id"></a> [vcd\_id](#output\_vcd\_id) | The Ionos VDC ID |
| <a name="output_vcd_location"></a> [vcd\_location](#output\_vcd\_location) | The Ionos VDC Location |
| <a name="output_vcd_name"></a> [vcd\_name](#output\_vcd\_name) | The Ionos VDC Name |
<!-- END OF PRE-COMMIT-OPENTOFU DOCS HOOK -->
