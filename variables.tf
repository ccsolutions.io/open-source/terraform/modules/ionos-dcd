variable "name" {
  description = "Ionos VDC Name"
  type        = string
}
variable "location" {
  description = "Ionos VDC Location"
  type        = string
}
variable "description" {
  description = "Short Description of the VDC, for example usage type or environments"
  type        = string
}
variable "sec_auth_protection" {
  description = "Enforce 2AF for the VDC"
  type        = bool
}
variable "size" {
  description = "(Required) The number of IP addresses to reserve for this block."
  type        = number
}
variable "ip_block_name" {
  description = "(Optional) The name of Ip Block"
  type        = string
}
variable "natgateway_name" {
  description = "(Required)[string] Name of the NAT gateway."
  type        = string
}