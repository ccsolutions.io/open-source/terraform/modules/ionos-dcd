output "vcd_name" {
  description = "The Ionos VDC Name"
  value       = ionoscloud_datacenter.datacenter.name
}
output "vcd_location" {
  description = "The Ionos VDC Location"
  value       = ionoscloud_datacenter.datacenter.location
}
output "vcd_id" {
  description = "The Ionos VDC ID"
  value       = ionoscloud_datacenter.datacenter.id
}
output "private_lan" {
  description = "The ID of the private lan"
  value       = ionoscloud_lan.private_lan.id
}
output "nodes_lan" {
  description = "The ID of the k8s-nodes lan"
  value       = ionoscloud_lan.nodes_lan.id
}
output "database_lan" {
  description = "The ID of the database lan"
  value       = ionoscloud_lan.database_lan.id
}
output "monitoring_lan" {
  description = "The ID of the monitoring lan"
  value       = ionoscloud_lan.monitoring_lan.id
}
output "storage_lan" {
  description = "The ID of the storage lan"
  value       = ionoscloud_lan.storage_lan.id
}
output "public_lan" {
  description = "The ID of the public lan"
  value       = ionoscloud_lan.public_lan.id
}
