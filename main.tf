resource "ionoscloud_datacenter" "datacenter" {
  name                = var.name
  location            = var.location
  description         = var.description
  sec_auth_protection = var.sec_auth_protection
}
resource "ionoscloud_ipblock" "example" {
  location = var.location
  size     = var.size
  name     = var.ip_block_name
}
resource "ionoscloud_lan" "private_lan" {
  datacenter_id = ionoscloud_datacenter.datacenter.id
  public        = false
  name          = "private"

  depends_on = [ionoscloud_datacenter.datacenter]
}
resource "ionoscloud_lan" "nodes_lan" {
  datacenter_id = ionoscloud_datacenter.datacenter.id
  public        = false
  name          = "nodes"

  depends_on = [ionoscloud_datacenter.datacenter]
}
resource "ionoscloud_lan" "database_lan" {
  datacenter_id = ionoscloud_datacenter.datacenter.id
  public        = false
  name          = "databases"

  depends_on = [ionoscloud_datacenter.datacenter]
}
resource "ionoscloud_lan" "monitoring_lan" {
  datacenter_id = ionoscloud_datacenter.datacenter.id
  public        = false
  name          = "monitoring"

  depends_on = [ionoscloud_datacenter.datacenter]
}
resource "ionoscloud_lan" "storage_lan" {
  datacenter_id = ionoscloud_datacenter.datacenter.id
  public        = false
  name          = "storage"

  depends_on = [ionoscloud_datacenter.datacenter]
}
resource "ionoscloud_lan" "public_lan" {
  datacenter_id = ionoscloud_datacenter.datacenter.id
  public        = true
  name          = "public"

  depends_on = [ionoscloud_datacenter.datacenter]
}
resource "ionoscloud_natgateway" "example" {
  datacenter_id = ionoscloud_datacenter.datacenter.id
  name          = var.natgateway_name
  public_ips    = [ionoscloud_ipblock.example.ips[0], ionoscloud_ipblock.example.ips[1]]
  lans {
    id          = ionoscloud_datacenter.datacenter.id
    gateway_ips = ["10.11.2.5"]
  }
}